# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-wireless/zd1211-firmware/zd1211-firmware-1.4.ebuild,v 1.2 2007/11/03 12:03:19 dsd Exp $

EAPI="3"

inherit eutils linux-mod toolchain-funcs

DESCRIPTION="PowerVR SGX540 libraries for OMAP4"

HOMEPAGE=""
BASE_URI="https://launchpad.net/ubuntu/+archive/primary/+files"
SRC_URI="${BASE_URI}/${PN}_${PV}.orig.tar.gz
${BASE_URI}/${PN}_${PV}-0ubuntu4.debian.tar.gz"


LICENSE="TI"
SLOT="0"
KEYWORDS="arm"

IUSE="debug"
DEPEND=">=x11-base/xorg-server-1.13
	x11-libs/libdrm
	media-libs/mesa
	x11-drivers/xf86-video-omap
	!x11-drivers/pvr-omap4-dkms"
RDEPEND="${DEPEND}"

S="${S}/sgx"
RESTRICT="strip"

pkg_pretend() {
	CTARGET=${CTARGET:-${CHOST}}
	if [[ $(tc-is-hardfloat) == no ]]; then
		eerror "Unfortunately this binary drivers are hardfloat only,"
		eerror "blame TI and Ubuntu. Reinstalling your system"
		eerror "with a hardfloat stage3 is recommended"
		die "Non-hardfloat toolchain detected"
	fi
}

src_prepare() {
	cd "${S}"
	epatch "${WORKDIR}"/debian/dkms/patches/*.patch
	cp "${WORKDIR}"/debian/Makefile .
}

src_compile() {
	emake -j1 KERNELDIR="${KV_DIR}" LDFLAGS="" FLAVOUR=release W=1  || die
}


src_install() {

	insinto /lib/modules/${KV_FULL}/kernel/extra
	doins omapdrm_pvr.ko

	cd "${WORKDIR}/${P}/usr"

	use debug && cp -r lib/debug/usr/lib/* lib/

	rm -rf lib/pkgconfig lib/debug

	cp -r . "${D}/usr"

#	emake DESTDIR="${D}" install

#	cd "${D}"
#	dodir usr/lib/opengl/pvr-omap4/lib
#	mv usr/lib/lib* usr/lib/opengl/pvr-omap4/lib/
#	mv usr/include usr/lib/opengl/pvr-omap4/
#	rm -rf usr/include/EGL \
#		usr/include/KHR \
#		usr/include/VG \
#		usr/lib/pkgconfig
#		usr/lib/libEGL.so* \
#		usr/lib/libOpenVG.so{,.1} \
#rm -rf		usr/lib/pkgconfig 
#	rm -rf "${D}"/usr/lib/libEGL.so.1 \
#		"${D}"/etc/init "${D}"/usr/lib/libOpenVG.so.1

}

