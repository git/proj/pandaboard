# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-wireless/zd1211-firmware/zd1211-firmware-1.4.ebuild,v 1.2 2007/11/03 12:03:19 dsd Exp $

EAPI="3"

inherit eutils linux-mod toolchain-funcs

DESCRIPTION="PowerVR SGX540 kernel driver for OMAP4"

HOMEPAGE=""
SRC_URI="https://launchpad.net/~tiomap-dev/+archive/release/+files/${PN}_${PV}.orig.tar.gz
https://launchpad.net/~tiomap-dev/+archive/release/+files/${PN}_${PV}-2.diff.gz"


LICENSE="TI"
SLOT="0"
KEYWORDS="arm"

IUSE=""
DEPEND="sys-kernel/omap4-sources"

S="${WORKDIR}/${P}/sgx"
#RESTRICT="strip"

pkg_pretend() {
	CTARGET=${CTARGET:-${CHOST}}
	if [[ $(tc-is-hardfloat) == no ]]; then
		eerror "Unfortunately this binary drivers are hardfloat only,"
		eerror "blame TI and Ubuntu. Reinstalling your system"
		eerror "with a hardfloat stage3 is recommended"
		die "Non-hardfloat toolchain detected"
	fi
}

src_prepare() {
	cd "${WORKDIR}"
	epatch *.diff
	mv git-import-orig/sgx/* ${P}/sgx
}

src_compile() {
	emake -j1 KERNELDIR="${KV_DIR}" LDFLAGS="" FLAVOUR=release W=1  || die
}

src_install() {
	insinto /lib/modules/${KV_FULL}/kernel/extra
	doins omapdrm_pvr.ko
}
